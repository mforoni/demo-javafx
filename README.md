# JavaFX Demos

Contains several JavaFX tutorials/demo applications:

* demo-javafx: code snippets from various websites
* [address-app](https://code.makery.ch/library/javafx-tutorial/part1/): an example of mvc application for managing personal addresses
* [oracle-javafx](http://docs.oracle.com/javafx/2/get_started/jfxpub-get_started.htm): contains code snippets from the official Oracle JavaFX tutorial.

## About JavaFX

JavaFX is a software platform for creating and delivering desktop applications, as well as rich internet applications (RIAs) that can run across a wide variety of devices. JavaFX is intended to replace Swing as the standard GUI library for Java SE, but both will be included for the foreseeable future.
