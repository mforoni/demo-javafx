/*
 * Example 12-12 Adding Map Data to the Table
 */
package com.gitlab.mforoni.oracle.javafx;

import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class TableViewSample2 extends Application {

  public static final String Column1MapKey = "A";
  public static final String Column2MapKey = "B";

  public static void main(final String[] args) {
    launch(args);
  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  @Override
  public void start(final Stage stage) {
    final Scene scene = new Scene(new Group());
    stage.setTitle("Table View Sample");
    stage.setWidth(300);
    stage.setHeight(500);
    final Label label = new Label("Student IDs");
    label.setFont(new Font("Arial", 20));
    final TableColumn<Map, String> firstDataColumn = new TableColumn<>("Class A");
    final TableColumn<Map, String> secondDataColumn = new TableColumn<>("Class B");
    firstDataColumn.setCellValueFactory(new MapValueFactory(Column1MapKey));
    firstDataColumn.setMinWidth(130);
    secondDataColumn.setCellValueFactory(new MapValueFactory(Column2MapKey));
    secondDataColumn.setMinWidth(130);
    final TableView table_view = new TableView<>(generateDataInMap());
    table_view.setEditable(true);
    table_view.getSelectionModel().setCellSelectionEnabled(true);
    table_view.getColumns().setAll(firstDataColumn, secondDataColumn);
    final Callback<TableColumn<Map, String>, TableCell<Map, String>> cellFactoryForMap =
        new Callback<TableColumn<Map, String>, TableCell<Map, String>>() {

          @Override
          public TableCell call(final TableColumn p) {
            return new TextFieldTableCell(new StringConverter() {

              @Override
              public String toString(final Object t) {
                return t.toString();
              }

              @Override
              public Object fromString(final String string) {
                return string;
              }
            });
          }
        };
    firstDataColumn.setCellFactory(cellFactoryForMap);
    secondDataColumn.setCellFactory(cellFactoryForMap);
    final VBox vbox = new VBox();
    vbox.setSpacing(5);
    vbox.setPadding(new Insets(10, 0, 0, 10));
    vbox.getChildren().addAll(label, table_view);
    ((Group) scene.getRoot()).getChildren().addAll(vbox);
    stage.setScene(scene);
    stage.show();
  }

  @SuppressWarnings("rawtypes")
  private ObservableList<Map> generateDataInMap() {
    final int max = 10;
    final ObservableList<Map> allData = FXCollections.observableArrayList();
    for (int i = 1; i < max; i++) {
      final Map<String, String> dataRow = new HashMap<>();
      final String value1 = "A" + i;
      final String value2 = "B" + i;
      dataRow.put(Column1MapKey, value1);
      dataRow.put(Column2MapKey, value2);
      allData.add(dataRow);
    }
    return allData;
  }
}
